import client from '../plugins/contentful'

export const state = () => ({
  links: []
})

export const mutations = {
  setContent(state, payload) {
    state.content = payload
  }
}

export const actions = {
  async getContent({ commit }) {
    await client
      .getEntries({
        content_type: 'homepage'
      })
      .then(response => {
        if (response.items.length > 0) {
          commit('setContent', response.items[0].fields)
        }
      })
  }
}
