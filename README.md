# danstuart

[![Netlify Status](https://api.netlify.com/api/v1/badges/0254dea1-bd6a-4f3d-b2cd-839de9552cd7/deploy-status)](https://app.netlify.com/sites/danstuart/deploys)

> My sweet Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
